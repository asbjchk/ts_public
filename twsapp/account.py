import time
import json
import pika

from common import *
from datetime import datetime

from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract

class AccountHandler(Handler, EWrapper, EClient):
    
    def __init__(self):
        EClient.__init__(self, self)
        self.logger = get_logger('TwsApp - Account', 'INFO')
        
        self.account_queue = os.getenv('ACCOUNT_QUEUE', 'account')

        self.channel = self.get_rmq_publisher_channel()
        self.channel.queue_declare(queue=self.account_queue, durable=True)
    
    #################
    ### Callbacks ###
    #################
    def updateAccountValue(self, key: str, val: str, currency: str, accountName: str):
        """
        account information including buying power, balances, margin requirements and PnLs
        to be inserted into influxdb
        """
        super().updateAccountValue(key, val, currency, accountName)
        if currency == 'HKD':
            if key in ['BuyingPower', 'CashBalance', 'TotalCashBalance', 'TotalCashValue',
                       'InitMarginReq', 'InitMarginReq-C', 'InitMarginReq-S',
                       'MaintMarginReq', 'MaintMarginReq-C', 'MaintMarginReq-S',
                       'RealizedPnL', 'UnrealizedPnL', 'FuturesPnL']:
                msg = {'tag'         : 'update_account_value',
                       'key'         : key,
                       'value'       : val,
                       'currency'    : currency,
                       'account_name': accountName,
                       'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
                self.logger.debug(f'receive updated account value: {msg}')
                self.channel.basic_publish(body=json.dumps(msg), exchange='', routing_key=self.account_queue,
                                           properties=pika.BasicProperties(delivery_mode=2)# make message persistent
                                          )
                self.logger.debug('pushed to account queue')

    def updatePortfolio(self, contract: Contract, position: float,
                        marketPrice: float, marketValue: float,
                        averageCost: float, unrealizedPNL: float,
                        realizedPNL: float, accountName: str):
        """
        current holdings information
        to be inserted into mysql
        """
        super().updatePortfolio(contract, position, marketPrice, marketValue,
                                averageCost, unrealizedPNL, realizedPNL, accountName)
        msg = {'tag'         : 'update_portfolio',
               'symbol'      : contract.localSymbol,
               'sec_type'    : contract.secType,
               'exchange'    : contract.exchange,
               'position'    : position,
               'market_price': marketPrice,
               'market_value': marketValue,
               'average_cost': averageCost,
               'unrealized_pnl': unrealizedPNL,
               'realized_pnl'  : realizedPNL,
               'account_name'  : accountName,
               'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
        self.logger.debug(f'receive updated portfolio: {msg}')
        self.channel.basic_publish(body=json.dumps(msg), exchange='', routing_key=self.account_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug('pushed to account queue')



