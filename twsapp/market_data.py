import time
import json
import pika

from common import *
from datetime import datetime
from math import floor

from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
from ibapi.ticktype import TickType, TickTypeEnum
from ibapi.common import TickerId, TickAttrib, TickAttribLast, TickAttribBidAsk

class MarketDataHandler(Handler, EWrapper, EClient):
    
    def __init__(self):
        EClient.__init__(self, self)
        self.logger = get_logger('TwsApp - Market Data', 'INFO')

        self.market_data_queue = os.getenv('MARKET_DATA_QUEUE', 'market_data')
        self.tick_data_queue   = os.getenv('TICK_DATA_QUEUE', 'tick_data')

        self._subscribed_data = {}
        
        self.channel = self.get_rmq_publisher_channel()
        self.channel.queue_declare(queue=self.tick_data_queue, durable=True)
        self.channel.queue_declare(queue=self.market_data_queue, durable=True)
        

    ################
    ### Requests ###
    ################
    def reqHsiTickData(self, localSymbols: dict):
        """
        FIXME: it's no longer HSI only (since 2020-12-27)
        Request market data of the HSI futures contracts:
            - request tick-by-tick data for front-month and next-month contracts
            - request 250 ms snapshot data for 5 nearest month contracts, 
              i.e. front, next, next two quarter months, and the following December month
    
        @param localSymbols (dict): localSymbols to subscribe, 
                                    e.g. {10 : 'HSIN0', 
                                          20 : 'HSIQ0', 
                                          30 : 'HSIU0', 
                                          60 : 'HSIZ0', 
                                          120: 'HSIZ1'}
        data_id CONVENTION: 
            <X>1 - trade tick data             , e.g. 11
            <X>2 - bid/ask tick data           , e.g. 12
            <X>3 - trade/bid/ask snapshot data , e.g. 13
        """
        if ('_subscribed_data' not in vars(self).keys()):
            self._subscribed_data = {}

        basic_info = {'secType'  : 'FUT',
                      'exchange' : 'HKFE',
                      'lastTradeDateOrContractMonth': None,
                      'strike': None,
                      'right' : None}

        contract = Contract()
        contract.secType  = basic_info['secType']
        contract.exchange = basic_info['exchange']
        for k, v in localSymbols.items():
            tmp = basic_info.copy()
            tmp['symbol']        = v[:-2]
            tmp['localSymbol']   = v
            contract.localSymbol = v
            if k in [10, 20, 30]:
                self.reqTickByTickData(int(k)+1, contract, "AllLast", 0, False)
                self.reqTickByTickData(int(k)+2, contract, "BidAsk", 0, False)
            self.reqMktData(int(k)+3, contract, "", False, False, [])
            self._subscribed_data[k] = tmp
        self.logger.debug(f'subscribed data: {self._subscribed_data}')

    def reqHsiOptionsMktData(self, lastTradeDateOrContractMonth: str, center_strike=23000):
        """
        Request market data of the HSI options contracts:
            - request 250 ms snapshot data for front month contracts that lies between center_strick +/- 2000)
    
        @param lastTradeDateOrContractMonth (str): last-trade-date of the contracts to subscribe, e.g. '20200730'
        @param center_strike (float): contracts that lie between center_strike +/- 2000 will be subscribed
        
        data_id CONVENTION: 
            <11><PRICE> +3 - call options with strike price = PRICE, trade/bid/ask snapshot data, 
                             e.g. 1123003 = snapshot data of call options with strike 23000
            <10><PRICE> +3 - put options with strike price = PRICE, trade/bid/ask snapshot data,
                             e.g. 1023003 = snapshot data of put options with strike 23,000
        """
        basic_info = {'secType'  : 'OPT',
                      'exchange' : 'HKFE',
                      'symbol'   : 'HSI',
                      'localSymbol': None,
                      'lastTradeDateOrContractMonth': lastTradeDateOrContractMonth}

        contract = Contract()
        contract.secType  = basic_info['secType']
        contract.exchange = basic_info['exchange']
        contract.symbol   = basic_info['symbol']
        contract.lastTradeDateOrContractMonth = basic_info['lastTradeDateOrContractMonth']

        center = floor(center_strike/200)*200   # HSI Option's strikes are separated by 200 points

        for k in range(center-2000, center+2000, 200):
            # Call
            contract.strike = k
            contract.right  = 'C'
            oid = 1100000 + k
            self.reqMktData(int(oid+3), contract, "", False, False, [])

            tmp = basic_info.copy()
            tmp['strike']   = k
            tmp['right']    = 'C'
            self._subscribed_data[oid] = tmp

            # Put
            contract.strike = k
            contract.right = 'P'
            oid = 1000000 + k
            self.reqMktData(int(oid+3), contract, "", False, False, [])

            tmp = basic_info.copy()
            tmp['strike']   = k
            tmp['right']    = 'P'
            self._subscribed_data[oid] = tmp
        self.logger.debug(f'subscribed data: {self._subscribed_data}')

    #################
    ### Callbacks ###
    #################
    @handle_error()
    def tickPrice(self, reqId: TickerId, tickType: TickType, price: float,  attrib: TickAttrib):
        """
        snapshot trade / bid / ask price
        """
        super().tickPrice(reqId, tickType, price, attrib)
        if tickType in [1, 2, 4]:
            dataId = reqId - 3
            msg = {'sec_type'    : self._subscribed_data[dataId]['secType'],
                   'exchange'    : self._subscribed_data[dataId]['exchange'],
                   'symbol'      : self._subscribed_data[dataId]['symbol'],
                   'local_symbol': self._subscribed_data[dataId]['localSymbol'],
                   'contract_month': self._subscribed_data[dataId]['lastTradeDateOrContractMonth'],
                   'strike'      : self._subscribed_data[dataId]['strike'],
                   'right'       : self._subscribed_data[dataId]['right'],
                   'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
                   'can_auto_execute' : attrib.canAutoExecute,
                   'past_limit'  : attrib.pastLimit,
                   'pre_open'    : attrib.preOpen}
            if tickType == 1: #TickTypeEnum.BID
                msg['bid_price']  = price
            if tickType == 2: #TickTypeEnum.ASK
                msg['ask_price']  = price
            if tickType == 4: #TickTypeEnum.LAST
                msg['price']  = price
            self.channel.basic_publish(body=json.dumps({'type': 'snapshot_price', 'item': msg}),
                                       exchange='',
                                       routing_key=self.market_data_queue,
                                       properties=pika.BasicProperties(delivery_mode=2))
        elif tickType in [6, 7, 9, 14]:
            return
        else:
            msg = {'reqId': reqId, 'tickType': tickType, 'price': price, 'attrib': attrib}
            self.logger.warning(f'Message of this tickType to be handled: {msg}')
            return

    @handle_error()
    def tickSize(self, reqId: TickerId, tickType: TickType, size: int):
        """
        snapshot trade / bid / ask size
        """
        super().tickSize(reqId, tickType, size)
        if tickType in [0, 3, 5]:
            dataId = reqId - 3
            msg = {'sec_type'    : self._subscribed_data[dataId]['secType'],
                   'exchange'    : self._subscribed_data[dataId]['exchange'],
                   'symbol'      : self._subscribed_data[dataId]['symbol'],
                   'local_symbol': self._subscribed_data[dataId]['localSymbol'],
                   'contract_month': self._subscribed_data[dataId]['lastTradeDateOrContractMonth'],
                   'strike'      : self._subscribed_data[dataId]['strike'],
                   'right'       : self._subscribed_data[dataId]['right'],
                   'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
            if tickType == 0: #TickTypeEnum.BID_SIZE
                    msg['bid_size']  = size
            if tickType == 3: #TickTypeEnum.ASK_SIZE
                    msg['ask_size']  = size
            if tickType == 5: #TickTypeEnum.LAST_SIZE
                    msg['size']  = size
            self.channel.basic_publish(body=json.dumps({'type': 'snapshot_size', 'item': msg}),
                                           exchange='',
                                           routing_key=self.market_data_queue,
                                           properties=pika.BasicProperties(delivery_mode=2))
        elif tickType in [8]:
            return
        else:
            msg = {'reqId': reqId, 'tickType': tickType, 'size': size}
            self.logger.warning(f'Message of this tickType to be handled: {msg}')
            return

    @handle_error()
    def tickByTickBidAsk(self, reqId: int, time: int, bidPrice: float, askPrice: float,
                         bidSize: int, askSize: int, tickAttribBidAsk: TickAttribBidAsk):
        """
        tick-by-tick bid/ask price and size
        """
        super().tickByTickBidAsk(reqId, time, bidPrice, askPrice, bidSize, askSize, tickAttribBidAsk)
        dataId = reqId - 2
        msg = {'sec_type'    : self._subscribed_data[dataId]['secType'],
               'exchange'    : self._subscribed_data[dataId]['exchange'],
               'symbol'      : self._subscribed_data[dataId]['symbol'],
               'local_symbol': self._subscribed_data[dataId]['localSymbol'],
               'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
               'broker_time' : datetime.fromtimestamp(time).strftime("%Y-%m-%d %H:%M:%S.%f"),
               'bid_price'   : bidPrice,
               'bid_size'    : bidSize,
               'bid_past_low': tickAttribBidAsk.bidPastLow,
               'ask_price'   : askPrice,
               'ask_size'    : askSize,
               'ask_past_high': tickAttribBidAsk.askPastHigh}
        self.channel.basic_publish(body=json.dumps({'type': 'bid_ask', 'item': msg}),
                                   exchange='',
                                   routing_key=self.tick_data_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))

    @handle_error()
    def tickByTickAllLast(self, reqId: int, tickType: int, time: int, price: float,
                          size: int, tickAtrribLast: TickAttribLast, exchange: str,
                          specialConditions: str):
        """
        tick-by-tick last price and size
        """
        super().tickByTickAllLast(reqId, tickType, time, price, size, tickAtrribLast, exchange, specialConditions)
        dataId = reqId - 1
        msg = {'sec_type'    : self._subscribed_data[dataId]['secType'],
               'exchange'    : self._subscribed_data[dataId]['exchange'],
               'symbol'      : self._subscribed_data[dataId]['symbol'],
               'local_symbol': self._subscribed_data[dataId]['localSymbol'],
               'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
               'broker_time' : datetime.fromtimestamp(time).strftime("%Y-%m-%d %H:%M:%S.%f"),
               'price'       : price, 
               'size'        : size,
               'spec_cond'   : specialConditions,
               'past_limit'  : tickAtrribLast.pastLimit,
               'unreported'  : tickAtrribLast.unreported}
        self.channel.basic_publish(body=json.dumps({'type': 'trade', 'item': msg}),
                                   exchange='',
                                   routing_key=self.tick_data_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))

if __name__ == '__main__':
    app = MarketDataHandler()
    app.connect("127.0.0.1", 3391, 0)
    app.reqHsiTickData({10 : 'HSIN0', 20 : 'HSIQ0', 30 : 'HSIU0', 60 : 'HSIZ0', 120: 'HSIZ1'})

    app.run()
