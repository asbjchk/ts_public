import os
import time
import json
import pika

from common import *
from datetime import datetime, timedelta
from threading import Thread

from market_data import *
from account import *
from order import *

LOGGER = get_logger('twsapp', level=str(os.getenv('LOG_LEVEL', 'INFO'))) # use globally (see the bottom)

class TwsApp(MarketDataHandler, AccountHandler, OrderHandler):

    def __init__(self):
        EClient.__init__(self, self)

        self.logger = LOGGER
        # read env var
        self.connection_ip        = str(os.getenv('IP', '127.0.0.1'))
        self.connection_port      = int(os.getenv('PORT', 3391))
        self.account_id           = os.getenv('ACCOUNT_ID', None)
        self.alert_queue          = os.getenv('ALERT_QUEUE', 'alert')

        self.connect_market_data  = str(os.getenv('CONNECT_MARKET_DATA')).lower() == 'true'
        self.market_data_queue    = os.getenv('MARKET_DATA_QUEUE', None)
        self.tick_data_queue      = os.getenv('TICK_DATA_QUEUE', None)

        self.connect_account      = str(os.getenv('CONNECT_ACCOUNT')).lower() == 'true'
        self.account_queue        = os.getenv('ACCOUNT_QUEUE', None)

        self.connect_order        = str(os.getenv('CONNECT_ORDER')).lower() == 'true'
        self.model_to_order_queue = os.getenv('MODEL_TO_ORDER_QUEUE', None)
        self.order_queue          = os.getenv('ORDER_QUEUE', None)

        # check env var
        self.logger.info(f"initialize: connection_ip='{self.connection_ip}', connection_port='{self.connection_port}', account_id='{self.account_id}', alert_queue='{self.alert_queue}'")
        if None in [self.connection_ip, self.connection_port]:
            raise Exception('environment variable IP or PORT is not defined')
        self.logger.info(f"initialize: connect_market_data='{self.connect_market_data}', market_data_queue='{self.market_data_queue}', tick_data_queue='{self.tick_data_queue}'")
        if self.connect_market_data and None in [self.market_data_queue, self.tick_data_queue]:
            raise Exception('Insufficient MARKET_DATA environment variables')
        self.logger.info(f"initialize: connect_account='{self.connect_account}', account_queue='{self.account_queue}'")
        if self.connect_account and None in [self.account_id, self.account_queue]:
            raise Exception('Insufficient ACCOUNT environment variables')
        self.logger.info(f"initialize: connect_order='{self.connect_order}', model_to_order_queue='{self.model_to_order_queue}', order_queue='{self.order_queue}'")
        if self.connect_order and None in [self.account_id, self.model_to_order_queue, self.order_queue]:
            raise Exception('Insufficient ORDER environment variables')

        # state variables
        self.nextOrderId = 0
        self._subscribed_data = {}
        self._open_orders = {} # will automatically updated by the tws when starting a connection

        # RabbitMQ publisher
        self.channel = self.get_rmq_publisher_channel()
        for q in [self.tick_data_queue, self.market_data_queue, self.account_queue, self.order_queue, self.model_to_order_queue, self.alert_queue]:
            if q is not None:
                self.channel.queue_declare(queue=q, durable=True)

        # RabbitMQ consumer (order related)
        self.consumer_channel = self.get_rmq_consumer_channel()
        self.consumer_channel.basic_consume(queue=self.model_to_order_queue, on_message_callback=self.route_model_to_order)

        # TODO: log the env var for debug

    def nextValidId(self, orderId: int):
        """
        + add the channel to tell the Alert Handler tws has been on.
        """
        super().nextValidId(orderId)
        self.channel.basic_publish(body=json.dumps({'type': 'tws_startup', 'item': f"NextValidId: {orderId}"}),
                                   exchange='',
                                   routing_key=self.alert_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))

    def error(self, reqId, errorCode, errorString: str):
        """
        print error messages returned by the tws

        Notification (tagged as INFO):
            2104: Market data farm connection is OK.
            2106: A historical data farm is connected.
            2108: A market data farm connection has become inactive but should be available upon demand.
            2158: Sec-def data farm connection is OK.

            2103: Market data farm connection is broken. (usually a false alert)
            2105: HMDS data farm connection is broken.   (usually a false alert)
            2157: Sec-def data farm connection is broken (usually a false alert)
            (we're relying on the Alert Handler to monitor the market data disconnection)

            1102: Connectivity between IB and TWS has been restored- data maintained.

        Ignored:
            10090: Part of requested market data is not subscribed.
                (This is mainly due to the subscription of the options data,
                 and we haven't subscribed any the underlying index data)
            1100 (20-21 HKT): Connectivity between IB and the TWS has been lost, due to IB server maintenance from 20:15 to 21:00 HKT
        """
        super().error(reqId, errorCode, errorString)

        NOTIFICATION_CODES = [2104, 2106, 2108, 2158, 2103, 2105, 2157, 1102]

        if errorCode in NOTIFICATION_CODES:
            self.logger.info(f'{errorString} (code: {errorCode})')
        elif errorCode == 10090 or (errorCode == 1100 and datetime.utcnow().hour == 12):
            self.logger.debug(f'{errorString} (code: {errorCode})')
        else:
            self.logger.error(f'reqId: {reqId}: {errorString} (code: {errorCode})')

    def get_hsi_last_price(self, local_symbol):
        """
        get HSI futures last price for options data subscription (to subscribe strikes within the price +/- 2000)
        """
        influxdb_reader = self.get_influxdb_reader()
        output = influxdb_reader.query(f"SELECT last(price) AS close FROM market_data.autogen.\"FUT/HKFE/HSI\" WHERE local_symbol = '{local_symbol}'")
        output = output.raw
        # {'statement_id': 0,
        #  'series': [{'name': 'FUT/HKFE/HSI',
        #              'columns': ['time', 'close'],
        #              'values': [['2020-07-10T18:59:58.899730Z', 25760.0]]}]}
        influxdb_reader.close()

        price = output['series'][-1]['values'][0][1]
        return price

    @handle_error(logger_name='twsapp-error')
    def main(self, date=None):
        """
        main of the whole twsapp
            - Read today data schedule from the mysql
            (if the data schedule is available, i.e. today is a trading day)
                - Connect to the TWS
                - Request HSI futures tick data and snapshot data
                - Read HSI futures last price from the influxdb
                - Request HSI options snapshot data
                - Request account update
                - Run the app inside the thread "api_thread"
                - Double-check the connection status
                - Start listening to the model_order queue
            (else, i.e. today is not a trading day)
                - Sleep the program for whole day
                - Exit the program
        """
        if date is None:
            date = datetime.utcnow().strftime('%Y-%m-%d')
        self.logger.info(f'Read data schedule: {date}')
        data_schedule  = self.get_data_schedule(date)

        if len(data_schedule) > 0:
            self.logger.info(f'data schedule: {data_schedule}')

            # connect and request data
            self.logger.info("Connecting...")
            self.connect(self.connection_ip, self.connection_port, 0)
            ## market data related
            if self.connect_market_data:
                self.reqHsiTickData({10: data_schedule['id_10'],
                                     20: data_schedule['id_20'],
                                     30: data_schedule['id_30'],
                                     60: data_schedule['id_60'],
                                     120: data_schedule['id_120']})
                self.logger.info('Read HSI last price for options data subscription')
                hsi_last_price = self.get_hsi_last_price(data_schedule['id_10'])
                self.logger.info(f"{data_schedule['id_10']} last price: {hsi_last_price}")
                self.reqHsiOptionsMktData(lastTradeDateOrContractMonth=data_schedule['id_1000000'],
                                          center_strike=hsi_last_price)
            ## account related
            if self.connect_account:
                self.reqAccountUpdates(True, self.account_id)

            ## order related
            if self.connect_order:
                # run the app in a thread
                api_thread = Thread(target=self.run)
                api_thread.start()

                time.sleep(5)

                # double-check connection
                connected = self.isConnected()
                if connected:
                    self.logger.info(f"Connection Status: {connected}")
                else:
                    self.logger.error(f"Connection Status: {connected}")

                # start listening to model orders
                self.consumer_channel.start_consuming()
            else:
                self.run()

        else:
            self.logger.info('No data schedule for today')

            # sleep until tmr
            now = datetime.utcnow()
            while now.strftime('%Y-%m-%d') == date:
                sleep_time = now.replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1) - now
                sleep_time = sleep_time.seconds
                if sleep_time > 10:
                    self.logger.info(f'Sleep {sleep_time - 10} seconds')
                    time.sleep(sleep_time - 10)
                now = datetime.utcnow()

        self.logger.warning('program exit!')

if __name__ == '__main__':
    while True:
        app = TwsApp()
        app.main(date=os.getenv('DATE'))
        del app
        LOGGER.warning('program exited, will restart in 60 seconds')
        time.sleep(60)
