import time
import json
import pika

from common import *
from datetime import datetime, timedelta

from ibapi.client import EClient
from ibapi.wrapper import EWrapper, OrderId, OrderState, Execution
from ibapi.contract import Contract
from ibapi.order import *

class OrderHandler(Handler, EWrapper, EClient):
    
    def __init__(self):
        EClient.__init__(self, self)
        self.logger = get_logger('TwsApp - Order', 'INFO')
        
        self.account_id    = os.getenv('ACCOUNT_ID', '')
        self.model_to_order_queue = os.getenv('MODEL_TO_ORDER_QUEUE', 'model_order')
        self.order_queue          = os.getenv('ORDER_QUEUE', 'order')

        self.nextOrderId = 0
        self._open_orders = {} # would recover by the tws on startup
        
        self.channel = self.get_rmq_publisher_channel()
        self.channel.queue_declare(queue=self.model_to_order_queue, durable=True)
        self.channel.queue_declare(queue=self.order_queue, durable=True)
    
    #################
    ### Callbacks ###
    #################
    def nextValidId(self, orderId: int):
        """
        next valid order id, being called on startup
        """
        super().nextValidId(orderId)
        self.nextOrderId = orderId
        self.logger.info(f"NextValidId: {orderId}")

    ## FIXME: error order handling
    def route_model_to_order(self, ch, method, properties, body):
        '''
        consume_model_messages_and_place_orders
        '''

        # TEST: supposed to be a thread safe connection / channel
        self.logger.debug('creating temporarily RMQ channel')
        connection = self.get_rmq_connection()
        channel = connection.channel()

        model_dicts    = []
        pending_orders = []

        ## Gather Messages
        self.logger.debug(f'receive orders from model_order queue: {body}')
        model_dicts.append(json.loads(body))
        ch.basic_ack(delivery_tag=method.delivery_tag)

        ## deModel
        for d in model_dicts:
            # publish to model map
            channel.basic_publish(body=json.dumps({'type': 'model', 
                                                   'item': {'orderId': self.nextOrderId, 
                                                            'model': d.pop('model'), 
                                                            'time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}}),
                                  exchange='',
                                  routing_key=self.order_queue,
                                  properties=pika.BasicProperties(delivery_mode=2))
            self.logger.debug('pushed "model" to order queue')
            d['orderId'] = self.nextOrderId
            pending_orders.append(d.copy())
            d['symbol'] = d['localSymbol']
            d['status']  = 'Created (internal)'
            d['time']    = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')

            # publish to order
            channel.basic_publish(body=json.dumps({'type': 'placeOrder', 'item': d}),
                                  exchange='',
                                  routing_key=self.order_queue,
                                  properties=pika.BasicProperties(delivery_mode=2))
            self.logger.debug(f'push "placeOrder" to order queue, msg: {d}')
            self.nextOrderId += 1

        ## TODO: Risk Check 

        ## Place Orders
        for d in pending_orders:
            contract = Contract()
            order    = Order()
            self.logger.debug(d)
            for k, v in d.items():
                self.logger.debug(f'%s: %s', k, v)
                if k in dir(contract):
                    self.logger.debug(f'Contract, %s: %s', k, v)
                    setattr(contract, k, v)
                if k in dir(order):
                    self.logger.debug(f'Order, %s: %s', k, v)
                    setattr(order, k, v)
            self.placeOrder(d['orderId'], contract, order)
        self.logger.debug('orders placed')

        self.logger.debug("deleting temporarily RMQ channel")
        channel.close()
        connection.close()

    def openOrder(self, orderId: OrderId, contract: Contract, order: Order, orderState: OrderState):
        super().openOrder(orderId, contract, order, orderState)
        d = {'orderId'  : orderId,
             'symbol'   : contract.localSymbol,
             'secType'  : contract.secType,
             'exchange' : contract.exchange,
             'tif'      : order.tif,
             'orderType': order.orderType,
             'action'   : order.action,
             'totalQuantity': order.totalQuantity,
             'lmtPrice' : order.lmtPrice,
             'auxPrice' : order.auxPrice,
             'status'   : orderState.status,
             'time'     : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
        self.logger.debug(f'receive open order: {d}')
        # update whole memory
        self._open_orders[d['orderId']] = d
        self.channel.basic_publish(body=json.dumps({'type': 'memory', 'item': self._open_orders}),
                                     exchange='',
                                     routing_key=self.order_queue,
                                     properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "memory" to order queue, msg: {self._open_orders}')
        # update order@DB
        self.channel.basic_publish(body=json.dumps({'type': 'openOrder', 'item': d}),
                                     exchange='',
                                     routing_key=self.order_queue,
                                     properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "openOrder" to order queue, msg: {d}')
        self.logger.info(f'Current Opening Orders: {self._open_orders}')

    def orderStatus(self, orderId: OrderId, status: str, filled: float,
                    remaining: float, avgFillPrice: float, permId: int,
                    parentId: int, lastFillPrice: float, clientId: int,
                    whyHeld: str, mktCapPrice: float):
        super().orderStatus(orderId, status, filled, remaining,
                            avgFillPrice, permId, parentId, lastFillPrice, clientId, whyHeld, mktCapPrice)
        d = {'orderId'  : orderId,
             'permId'   : permId,
             'clientId' : clientId,
             'filled'   : filled,
             'lastFillPrice': lastFillPrice,
             'avgFillPrice' : avgFillPrice,
             'remaining': remaining,
             'status'   : status,
             'time'     : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
        self.logger.debug(f'receive order status: {d}')
        # update memory
        if d['orderId'] in self._open_orders.keys():
            if d['status'] == 'Filled' or d['status'] == 'Cancelled':
                self._open_orders.pop(d['orderId'])
            else:
                self._open_orders[d['orderId']]['status'] = d['status']
            self.channel.basic_publish(body=json.dumps({'type': 'memory', 'item': self._open_orders}),
                                         exchange='',
                                         routing_key=self.order_queue,
                                         properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "memory" to order queue, msg: {self._open_orders}')
        # update orderStatus@DB
        self.channel.basic_publish(body=json.dumps({'type': 'orderStatus', 'item': d}),
                                     exchange='',
                                     routing_key=self.order_queue,
                                     properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "orderStatus" to order queue, msg: {d}')
        self.logger.info(f'Current Opening Orders: {self._open_orders}')

    def execDetails(self, reqId: int, contract: Contract, execution: Execution):
        super().execDetails(reqId, contract, execution)
        d = {'orderId'  : execution.orderId,
             'permId'   : execution.permId,
             'clientId' : execution.clientId,
             'filled'   : execution.cumQty,
             'lastFillPrice': execution.price,
             'avgFillPrice' : execution.avgPrice,
             'execId'   : execution.execId,
             'remark'   : 'exec',
             'time'     : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
             'remaining': self._open_orders[execution.orderId]['totalQuantity'] - execution.cumQty}
        d['status'] = 'Filled' if d['remaining'] == 0 else 'Submitted'
        self.logger.debug(f'receive exec details: {d}') 
        # update memory
        if d['orderId'] in self._open_orders.keys():
            if d['status'] == 'Filled' or d['status'] == 'Cancelled':
                self._open_orders.pop(d['orderId'])
            else:
                self._open_orders[d['orderId']]['status'] = d['status']
            self.channel.basic_publish(body=json.dumps({'type': 'memory', 'item': self._open_orders}),
                                         exchange='',
                                         routing_key=self.order_queue,
                                         properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "memory" to order queue, msg: {self._open_orders}')
        # update orderStatus@DB
        self.channel.basic_publish(body=json.dumps({'type': 'execDetails', 'item': d}),
                                   exchange='',
                                   routing_key=self.order_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))
        self.logger.debug(f'pushed "execDetails" to order queue, msg: {d}')
        self.logger.info(f'Current Opening Orders: {self._open_orders}')


if __name__ == '__main__':
    app = OrderHandler()
    app.connect("127.0.0.1", 3391, 0)

    app.run()
