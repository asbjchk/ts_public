import os
import pickle
import pandas as pd

from common import *
from datetime import datetime, timedelta
from googleapiclient.discovery import build

class GoogleSheetUpdater(Handler):
    logger = get_logger('update_google_sheet', level=str(os.getenv('LOG_LEVEL', 'INFO')))
    
    def __init__(self):
        self.account_name = str(os.getenv('ACCOUNT_NAME', None))
        self.sheet_id     = str(os.getenv('SHEET_ID', None))
        self.sheet_name   = str(os.getenv('SHEET_NAME', None))
        self.logger.info(f"initialize GoogleSheetUpdater: account_name='{self.account_name}', sheet_id='{self.sheet_id}', sheet_name='{self.sheet_name}'")
        
        # influxdb
        self.influxdb_reader = self.get_influxdb_reader(database='account')

    def get_total_cash_value(self, date):
        tmp = self.influxdb_reader.query(f'SELECT last("TotalCashValue") FROM "account"."autogen"."account_value" WHERE time > \'{date} 00:00:00\' AND time < \'{date} 23:59:59\' AND account_name=\'{self.account_name}\'')
        if (len(list(tmp)) == 0):
            self.logger.warning(f'InfluxDB returned no results from {date}')
            return None
            
        tmp = list(tmp)[0][0]    # {'time': '2019-12-15T18:14:00Z', 'last': 7869120.69}
        value = [[tmp['time'][0:10], tmp['last'], '', tmp['time']]]
        body = {'values': value} # {'values': [['2019-12-15', 7869120.69, '', '2019-12-15T18:14:00Z']]}
        self.logger.info(body)
        return body

    @schedule(str(os.getenv('CRON', '23 * * * * 59')), logger)
    @handle_error()
    def main(self):
        '''
        update google-drive://Value_Investment/admin/account
        '''
        date = datetime.utcnow().date()
        updated_cash_value = self.get_total_cash_value(date)
        if updated_cash_value is None:
            return
        
        # access google sheet
        self.logger.debug('access google sheet')
        with open('token.pickle', 'rb') as token:
            self.logger.debug('reading token.pickle ...')
            creds = pickle.load(token)
        service = build('sheets', 'v4', credentials=creds)
        sheets  = service.spreadsheets().values()

        # get index number for update
        date_column = sheets.get(range=f'{self.sheet_name}!A:A', spreadsheetId=self.sheet_id).execute()
        try:
            idx_to_update = 1 + date_column['values'].index([str(date)])
        except:
            idx_to_update = 1 + len(date_column['values'])
        self.logger.info(f'update {self.sheet_name}!A{idx_to_update}')

        # update total cash value
        sheets.update(
            body=updated_cash_value,
            range=f'{self.sheet_name}!A{idx_to_update}',
            spreadsheetId=self.sheet_id,
            valueInputOption='USER_ENTERED'
        ).execute()
        
if __name__ == '__main__':
    updater = GoogleSheetUpdater()
    updater.main()
