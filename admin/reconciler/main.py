import os
import pandas as pd

from common import *
from datetime import datetime, timedelta

class Reconciler(Handler):
    logger = get_logger('reconciler', level=str(os.getenv('LOG_LEVEL', 'INFO')))
    
    def __init__(self):
        self.from_mysqldb = str(os.getenv('FROM_MYSQLDB', 'order'))
        self.to_influxdb  = str(os.getenv('TO_INFLUXDB', 'reconciler'))
        self.logger.info(f"initialize Reconciler: from_mysqldb='{self.from_mysqldb}', to_influxdb='{self.to_influxdb}'")

        # influxdb
        self.influxdb_reader = self.get_influxdb_reader()
        self.influxdb_writer = self.get_influxdb_writer(database=self.to_influxdb)
        self.influxdb_writer.create_database(self.to_influxdb)
        self.influxdb_writer.grant_privilege(privilege='read', database=self.to_influxdb, username='reader')
        self.influxdb_writer.grant_privilege(privilege='read', database=self.to_influxdb, username='visitor')
        
        # for get_next_trading_date
        self.trading_dates = []
    
    def get_hsi_last_price(self, local_symbol, snapshot=datetime.utcnow()):
        """
        get HSI futures last price at a specific snapshot
        
        @param local_symbol(str)
        @param snapshot(datetime)
        """

        output = self.influxdb_reader.query(f"SELECT last(price) AS close FROM market_data.autogen.\"FUT/HKFE/HSI\" WHERE local_symbol = '{local_symbol}' AND time <= '{str(snapshot)}'")
        output = output.raw
        # {'statement_id': 0,
        #  'series': [{'name': 'FUT/HKFE/HSI',
        #              'columns': ['time', 'close'],
        #              'values': [['2020-07-10T18:59:58.899730Z', 25760.0]]}]}
        #influxdb_reader.close()
        
        price = output['series'][-1]['values'][0][1]    
        return price
    
    def get_next_trading_date(self, date):
        """
        get the trading date next to the specified date
        
        @param date(same as those in self.trading_dates)
        """
        for i, v in enumerate(self.trading_dates):
            if v == date and i < len(self.trading_dates) - 1:
                return self.trading_dates[i+1]
        return None

    # before 2020-06-17, commission = 30
    # on/after 2020-06-17, commission = 19.04
    @handle_error()
    def calc_models_daily_pnl(self, snapshot=datetime.utcnow(), look_back_day=0, commission=19.04):
        """
        @param snapshot(datetime) : UTC snapshot
        @param look_back_day(int) : look back days for calculating the daily pnl and daily open position
        @param commission(float)  : fix-rate: $30 (before 2020-06-27), tier-rate: $19.04 (on/after 2020-06-27)
        
        @return res(dict): 
            res['time']     : snapshot time
            res['trade']    : detail trade info
            res['hist_pnl'] : historical daily open position, earned points, commission, pnl By model, symbol, date
            res['pnl']      : (at snapshot day) open position, earned points, commission, pnl By model, symbol, date
        """
        # read transaction data from mysql
        sql_reader = self.get_mysql_reader()
        cur        = sql_reader.cursor()
        self.logger.debug(f'reading transaction data ({str(snapshot)}) from mysql')
        start_time = str((snapshot - timedelta(days=look_back_day)).date()) + ' 00:00:00'
        filter_ = f"WHERE time >= '{str(start_time)}' AND time <= '{str(snapshot)}'"
        self.logger.debug(f'filter: {filter_}')
        cur.execute(f"SELECT * FROM {self.from_mysqldb}.model " + filter_)
        model = pd.DataFrame(cur.fetchall(), columns=[x[0] for x in cur.description])
        self.logger.debug(model)
        cur.execute(f"SELECT * FROM {self.from_mysqldb}.order " + filter_)
        order = pd.DataFrame(cur.fetchall(), columns=[x[0] for x in cur.description])
        self.logger.debug(order)
        cur.execute(f"SELECT * FROM {self.from_mysqldb}.order_status " + filter_)
        order_status = pd.DataFrame(cur.fetchall(), columns=[x[0] for x in cur.description])
        self.logger.debug(order_status)
        sql_reader.close()
        if len(model) == 0 or len(order) == 0 or len(order_status) == 0:
            self.logger.debug('no trade data available yet')
            return
        
        # aggregate trade data
        self.logger.debug('aggregating trade data')
        model['date'] = model['time'].dt.date
        order['date'] = order['time'].dt.date
        order = order[order['status']=='Filled'].groupby(['date', 'order_id']).last().reset_index()
        order_status['date'] = order_status['time'].dt.date
        order_status = order_status[order_status['status']=='Filled'].groupby(['date', 'order_id']).last().reset_index()
        
        trade = model.rename(columns={'time': 'sent_time'}).merge(
            order.loc[:,['date', 'order_id', 'symbol', 'action', 'time']].rename(columns={'time': 'exec_time'}),
            on=['date', 'order_id'],
            how="outer")
        trade = trade.merge(
            order_status.loc[:,['date', 'order_id', 'filled', 'avg_fill_price', 'time', 'remark']].rename(columns={'time': 'confirm_time'}),
            on=['date', 'order_id'],
            how="outer")
        
        trade = trade[trade['filled'] == trade['filled']] # clean rows that contains NaN
        trade['sold_qty']   = trade['filled'] * (2*(trade['action'] == 'SELL') - 1)
        trade['commission'] = trade['filled'] * commission
        
        # for self.get_next_trading_date
        self.trading_dates = trade['date'].unique()

        # complete pnl(=0) for all models for each trading day
        self.logger.debug('preprocessing...')
        tmp = pd.DataFrame([(x, y) for x in trade['date'].unique() for y in trade['model'].unique()], columns=['date', 'model'])
        trade = tmp.merge(trade, on=['date', 'model'], how='left')
        trade['symbol']         = trade['symbol'].fillna(method='ffill')
        trade['avg_fill_price'] = trade['avg_fill_price'].fillna(0)
        trade['sold_qty']       = trade['sold_qty'].fillna(0)
        trade['commission']     = trade['commission'].fillna(0)

        # calculate daily position by model
        self.logger.debug('calclating daily position by model')
        def f(df):
            df['position'] = (-df['sold_qty']).cumsum()
            return df.loc[:,['date','sold_qty', 'position']].reset_index(drop=True)

        position = trade.groupby(['model', 'symbol']).apply(f)
        position = position.groupby(['date', 'model', 'symbol']).apply(
            lambda x: pd.Series({'position': float(x['position'].tail(1))})
        )
        position = position.reset_index()
        
        # add market close/open price for daily open positions
        self.logger.debug('adding market close/open price for daily open positions (if any)')
        syn_trades = []
        for i in range(position.shape[0]):
            if position['position'][i] != 0:
                snapshot_tmp   = min(str(position['date'][i]) + ' 23:59:00', str(snapshot))
                hsi_last_price = self.get_hsi_last_price(local_symbol=position['symbol'][i], snapshot=snapshot_tmp)
                closing = {
                     'order_id'  : 99999,
                     'model'     : position['model'][i],
                     'date'      : position['date'][i],
                     'symbol'    : position['symbol'][i],
                     'avg_fill_price': hsi_last_price,
                     'confirm_time'  : snapshot_tmp,
                     'remark'    : 'marketClose',
                     'sold_qty'  : position['position'][i],
                     'commission': 0
                }
                opening = closing.copy()
                opening['order_id'] = -1
                opening['date']     = self.get_next_trading_date(opening['date'])
                opening['remark']   = 'marketOpen'
                opening['sold_qty'] = -opening['sold_qty']
                syn_trades.append(closing)
                syn_trades.append(opening)
        
        trade = pd.concat([trade, pd.DataFrame(syn_trades)], axis=0)
        trade = trade.sort_values(['date', 'order_id'])
        
        # calculate daily pnl
        self.logger.debug('calculating daily pnl')
        pnl = trade.groupby(['date', 'model', 'symbol']).apply(lambda x: pd.Series({'points': sum(x['sold_qty'] * x['avg_fill_price']), 'commission': sum(x['commission'])})).reset_index()
        pnl['pnl'] = pnl['points'] * 50 - pnl['commission']
        pnl = position.merge(pnl, on=['date', 'model', 'symbol'], how='left')
        
        return {'time': snapshot, 'trade': trade, 'hist_pnl': pnl, 
                'pnl': pnl[pnl['date'] == snapshot.date()].reset_index(drop=True)}
    
    @schedule(str(os.getenv('CRON', '* * * * * 0')), logger)
    @handle_error()
    def main(self):
        now = datetime.utcnow()
        self.logger.debug(now)
        res = self.calc_models_daily_pnl(snapshot = now, look_back_day=1)
        self.logger.info('done!')
        if res is None:
            return
        
        pnl = res['pnl'].to_dict('record')
        if len(pnl) == 0:
            return
                
        points = [{'measurement': 'daily_pnl',
                   'tags': {'model' : x.pop('model'),
                            'symbol': x.pop('symbol'),
                            'date'  : x.pop('date')},
                   'time': res['time'],
                   'fields': x
                  } for x in pnl]
        self.logger.debug(points)
        self.influxdb_writer.write_points(points)
        self.logger.info(f'written {len(points)} points into influxdb')
        
    @handle_error()
    def backfill(self, start, end, cron='0 * * * * 0', commission=19.04):
        """
        backfill the snpshot daily pnl in the influxdb
        
        @param start(datetime) : start snapshot time (UTC)
        @param end(datetime)   : end snapshot time (UTC)
        @param cron(str)       : cron schedule
        """
        _iter = croniter(cron, start)
        next_run_time = start
        
        while next_run_time <= end:
            self.logger.info(f'reproducing snapshot: {str(next_run_time)}')
            res = self.calc_models_daily_pnl(snapshot = next_run_time, commission = commission)
            if res is None:
                next_run_time = _iter.get_next(datetime)
                continue
            
            pnl = res['pnl'].to_dict('record')
            if len(pnl) == 0:
                next_run_time = _iter.get_next(datetime)
                continue
                
            points = [{'measurement': 'daily_pnl',
                       'tags': {'model' : x.pop('model'),
                                'symbol': x.pop('symbol'),
                                'date'  : x.pop('date')},
                       'time': res['time'],
                       'fields': x
                      } for x in pnl]
            self.influxdb_writer.write_points(points)
            self.logger.info(f'written {len(points)} points into influxdb')
            
            next_run_time = _iter.get_next(datetime)
        
if __name__ == '__main__':
    reconciler = Reconciler()
    reconciler.main()
