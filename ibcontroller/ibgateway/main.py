import os
import subprocess

from common import *
from threading import Thread

class IbGatewayController(Handler):
    logger = get_logger('ibgateway-controller', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        self.logger.info(f"initialize IB Gateway Controller: (account: {os.getenv('USERNAME')})")
        
        self.pid = 0

    @schedule(str(os.getenv('CHECK_CRON', '* * * * * 0-59/30')), logger, buffer=1, mute=True)
    @handle_error(logger_name='ibgateway-controller-error')
    def check_process(self):
        proc = subprocess.check_output(['ps', 'aux'])
        proc = proc.decode('utf-8').split('\n')
        ib_proc = [p for p in proc if 'java' in p and 'Sl' in p]
        
        if len(ib_proc) > 0:
            pids = [int([x for x in p.split(' ') if x != ''][1]) for p in ib_proc]
            
            if self.pid in pids: # if self.pid exists, then ok
                self.logger.debug(f'PID {self.pid} is running')
            
            elif len(pids) == 1: # pid changes due to restart, then update the pid
                self.logger.warning(f'process restarted, new PID = {pids[0]}')
                self.pid = pids[0] 
                
            else: # self.pid doesn't exist and multiple unknown pids, then unable to track
                self.logger.error('failed to track the process')
                for p in ib_proc:
                    self.logger.info(p[:81])
                self.logger.info(f'(PID under monitoring: {self.pid})')
        else:
            self.logger.error('process shutdown')
            self.pid = subprocess.run(['./gatewaystart.sh']).returncode
            self.logger.warning(f'restart process, new PID = {self.pid}')
            
    
    @schedule(str(os.getenv('PRINT_CRON', None)), logger)
    @handle_error(logger_name='ibgateway-controller-error')
    def print_process(self):
        proc = subprocess.check_output(['ps', 'aux'])
        proc = proc.decode('utf-8').split('\n')
        ib_proc = [p for p in proc if 'java' in p]
        
        self.logger.info(proc[0])
        for p in ib_proc:
            self.logger.info(p[:81])

    @handle_error(logger_name='ibgateway-controller-error')
    def main(self):
        """
        - start ib gateway process
        - check periodically the existence of the process
        - print periodically the status of the process
        """
        # run gatewaystart.sh and get the pid
        self.pid = subprocess.run(['./gatewaystart.sh']).returncode
        
        # print process
        if os.getenv('PRINT_CRON', None) is not None:
            print_thread = Thread(target=self.print_process)
            print_thread.start()
            
        # check process
        self.check_process()
        
    
if __name__ == '__main__':
    handler = IbGatewayController()
    handler.main()
