import os
import json

from common import *

class MarketDataConsumer(Consumer):
    '''
    Consumer for the snapshot market data
    '''
    logger = get_logger('market-data-consumer', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        super().__init__(from_queue=str(os.getenv('FROM_QUEUE', 'market_data')))
        self.influxdb = str(os.getenv('INFLUXDB', 'market_data'))
        self.batch_size = int(os.getenv('BATCH_SIZE', 1))
        self.alert_queue = str(os.getenv('ALERT_QUEUE', 'alert'))
        self.logger.info(f"initialize MarketDataConsumer: from_queue='{self.from_queue}', influxdb='{self.influxdb}', batch_size='{self.batch_size}', alert_queue='{self.alert_queue}'")

        self.influxdb_writer = self.get_influxdb_writer(database=self.influxdb)
        ## TODO: put the db creation into influxdb init
        self.influxdb_writer.create_database(self.influxdb)
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='reader')
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='visitor')
        ##

        self.alert_channel = self.get_rmq_publisher_channel()
        self.alert_channel.queue_declare(queue=self.alert_queue, durable=True)

        self.points = []

    @handle_error(logger_name='market-data-consumer-error')
    def callback(self, ch, method, properties, body):
        self.logger.debug(f'receive from market_data queue: {body}')

        msg = json.loads(body)
        msg_type = msg['type']
        item = msg['item']

        if msg_type == 'snapshot_price':
            self.points.append({'measurement': '/'.join([item.pop('sec_type'), item.pop('exchange'), item.pop('symbol')]),
                                'tags': {'local_symbol' : item.pop('local_symbol'),
                                         'contract_month': item.pop('contract_month'),
                                         'strike'       : item.pop('strike'),
                                         'right'        : item.pop('right'),
                                         'past_limit'   : item.pop('past_limit'),
                                         'pre_open'     : item.pop('pre_open'),
                                         'tick_by_tick' : False},
                                'time': item.pop('time'),
                                'fields': item
                              })
        elif msg_type == 'snapshot_size':
            self.points.append({'measurement': '/'.join([item.pop('sec_type'), item.pop('exchange'), item.pop('symbol')]),
                                'tags': {'local_symbol' : item.pop('local_symbol'),
                                         'contract_month': item.pop('contract_month'),
                                         'strike'       : item.pop('strike'),
                                         'right'        : item.pop('right'),
                                         'tick_by_tick' : False},
                                'time': item.pop('time'),
                                'fields': item
                              })
        else:
            self.logger.warning(f'Message to be handled: {msg}')

        self.logger.debug(f'points size: {len(self.points)}')
        if (len(self.points) >= self.batch_size):
            self.influxdb_writer.write_points(self.points)
            self.logger.debug(f'written {len(self.points)} points into influxdb')
            self.alert_channel.basic_publish(body=json.dumps({'type': 'market_data', 'item': f'written {len(self.points)} points into influxdb'}),
                                             exchange='',
                                             routing_key=self.alert_queue,
                                             properties=pika.BasicProperties(delivery_mode=2))
            self.points = []
            self.logger.debug(f'reset points, points size: {len(self.points)}')

if __name__ == '__main__':
    consumer = MarketDataConsumer()
    consumer.main()
