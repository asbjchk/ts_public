import os
import json

from common import *

class TickDataConsumer(Consumer):
    '''
    Consumer for the tick-by-tick data
    '''
    logger = get_logger('tick-data-consumer', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        super().__init__(from_queue=str(os.getenv('FROM_QUEUE', 'tick_data')))
        self.influxdb = str(os.getenv('INFLUXDB', 'market_data'))
        self.batch_size = int(os.getenv('BATCH_SIZE', 5))
        self.alert_queue = str(os.getenv('ALERT_QUEUE', 'alert'))
        self.logger.info(f"initialize TickDataConsumer: from_queue='{self.from_queue}', influxdb='{self.influxdb}', batch_size='{self.batch_size}', alert_queue='{self.alert_queue}'")

        self.influxdb_writer = self.get_influxdb_writer(database=self.influxdb)
        ## TODO: put the db creation into influxdb init
        self.influxdb_writer.create_database(self.influxdb)
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='reader')
        ##

        self.alert_channel = self.get_rmq_publisher_channel()
        self.alert_channel.queue_declare(queue=self.alert_queue, durable=True)

        self.points = []

    @handle_error(logger_name='tick-data-consumer-error')
    def callback(self, ch, method, properties, body):
        self.logger.debug(f'receive from tick_data queue: {body}')

        msg = json.loads(body)
        msg_type = msg['type']
        item = msg['item']

        if msg_type == 'trade':
            #{"type": "trade", "item": {"sec_type": "FUT", "exchange": "HKFE", "symbol": "HSI", "local_symbol": "HSIU0", "time": "2020-03-10 05:29:16.655488", "broker_time": "20200310 05:29:16.000000", "price": 24848.0, "size": 1, "spec_cond": "", "past_limit": false, "unreported": false}}
            self.points.append({'measurement': '/'.join([item.pop('sec_type'), item.pop('exchange'), item.pop('symbol')]),
                                'tags': {'local_symbol'      : item.pop('local_symbol'),
                                         'special_conditions': item.pop('spec_cond'),
                                         'past_limit'        : item.pop('past_limit'),
                                         'unreported'        : item.pop('unreported'),
                                         'tick_by_tick'      : True},
                                'time': datetime.strptime(item.pop('time'), '%Y-%m-%d %H:%M:%S.%f'),
                                'fields': item
                               })
        elif msg_type == 'bid_ask':
            #{"type": "bid_ask", "item": {"sec_type": "FUT", "exchange": "HKFE", "symbol": "HSI", "local_symbol": "HSIH0", "time": "2020-03-10 05:29:33.530421", "broker_time": "20200310 05:29:32.000000", "bid_price": 25471.0, "bid_size": 3, "bid_past_low": false, "ask_price": 25473.0, "ask_size": 1, "ask_past_high": false}}
            self.points.append({'measurement': '/'.join([item.pop('sec_type'), item.pop('exchange'), item.pop('symbol')]),
                                'tags': {'local_symbol' : item.pop('local_symbol'),
                                         'bid_past_low' : item.pop('bid_past_low'),
                                         'ask_past_high': item.pop('ask_past_high'),
                                         'tick_by_tick' : True},
                                'time': datetime.strptime(item.pop('time'), '%Y-%m-%d %H:%M:%S.%f'),
                                'fields': item
                              })
        else:
            self.logger.warning(f'Message to be handled: {msg}')


        self.logger.debug(f'points size: {len(self.points)}')
        if (len(self.points) >= self.batch_size):
            self.influxdb_writer.write_points(self.points)
            self.logger.info(f'written {len(self.points)} points into influxdb')
            self.alert_channel.basic_publish(body=json.dumps({'type': 'market_data', 'item': f'written {len(self.points)} points into influxdb'}),
                                             exchange='',
                                             routing_key=self.alert_queue,
                                             properties=pika.BasicProperties(delivery_mode=2))
            self.points = []
            self.logger.debug(f'reset points, points size: {len(self.points)}')

if __name__ == '__main__':
    consumer = TickDataConsumer()
    consumer.main()
