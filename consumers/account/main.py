import os
import json

from common import *

class AccountConsumer(Consumer):
    '''
    Consumer for the account data
    '''
    logger = get_logger('account-consumer', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        super().__init__(from_queue=str(os.getenv('FROM_QUEUE', 'account')))
        self.influxdb = str(os.getenv('INFLUXDB', 'account'))
        self.mysqldb  = str(os.getenv('MYSQLDB', 'position'))
        self.logger.info(f"initialize AccountConsumer: from_queue='{self.from_queue}', influxdb='{self.influxdb}', mysqldb='{self.mysqldb}'")

        self.influxdb_writer = self.get_influxdb_writer(database=self.influxdb)
        ## TODO: put the db creation into influxdb init
        self.influxdb_writer.create_database(self.influxdb)
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='reader')
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='visitor')
        ##
        self.mysql_writer    = self.get_mysql_writer()
        self.cur = self.mysql_writer.cursor()
 
    @handle_error(logger_name='account-consumer-error')
    def callback(self, ch, method, properties, body):
        self.logger.debug(f'receive from account queue: {body}')
        msg = json.loads(body)

        if msg['tag'] == 'update_account_value':
            # {'tag': 'update_account_value', 'key': 'CashBalance', 'value': '65768.2422', 'currency': 'HKD', 'account_name': 'DU1726767'}
            point = [{'measurement': msg['tag'].replace('update_', ''),
                      'tags': {'currency': msg['currency'],
                               'account_name': msg['account_name']},
                      'time': msg['time'],
                      'fields': { msg['key']: float(msg['value']) }
                     }]
            self.influxdb_writer.write_points(point)
            self.logger.debug(f'written into influxdb: {point}')

        elif msg['tag'] == 'update_portfolio':
            # {'tag': 'update_portfolio', 'symbol': 'HSIF0', 'sec_type': 'FUT', 'exchange': '', 'position': 1.0, 'market_price': 27547.96484375, 'market_value': 1377398.24, 'average_cost': 1311630.0, 'unrealized_pnl': 65768.24, 'realized_pnl': 0.0, 'account_name': 'DU1726767'}
            self.cur.execute(f"""INSERT INTO `{self.mysqldb}`.`account_total` (time, exchange, sec_type, symbol, position)
                                 VALUES (%s, %s, %s, %s, %s)""",
                              (msg['time'], msg['exchange'], msg['sec_type'], msg['symbol'], msg['position']))
            self.mysql_writer.commit()
            self.logger.debug(f'written into mysql: {msg}')

        elif msg['tag'] == 'update_account_time':
            return
        else:
            self.logger.warning(f'Message to be handled: {msg}')

if __name__ == '__main__':
    consumer = AccountConsumer()
    consumer.main()
