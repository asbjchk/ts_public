import os
import json

from common import *

class OrderConsumer(Consumer):
    '''
    Consumer for the order record data
    '''
    logger = get_logger('order-consumer', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        super().__init__(from_queue=str(os.getenv('FROM_QUEUE', 'order')))
        self.mysqldb  = str(os.getenv('MYSQLDB', 'order'))
        self.logger.info(f"initialize OrderConsumer, from_queue='{self.from_queue}', mysqldb='{self.mysqldb}'")

        self.mysql_writer = self.get_mysql_writer()
        self.cur = self.mysql_writer.cursor()

    @handle_error(logger_name='order-consumer-error')
    def callback(self, ch, method, properties, body):
        self.logger.debug(f'receive from order queue: {body}')
        msg = json.loads(body)
        msg_type = msg['type']
        item = msg['item']

        if msg_type == 'model':
            self.cur.execute(f"INSERT INTO `{self.mysqldb}`.`model` VALUES (%s, %s, %s)", (item['orderId'], item['model'], item['time']))
            self.logger.debug('pending to `model` table')

        if msg_type == 'placeOrder' or msg_type == 'openOrder':
            self.cur.execute(f"""INSERT INTO `{self.mysqldb}`.`order` (order_id, symbol, sec_type, exchange, tif, order_type, action, total_quantity, status, time)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                            (item['orderId'], item['symbol'], item['secType'], item['exchange'], item['tif'],
                             item['orderType'], item['action'], item['totalQuantity'], item['status'], item['time']))

            if 'lmtPrice' in item.keys():
                self.cur.execute(f"UPDATE `{self.mysqldb}`.`order` SET lmt_price = %s WHERE order_id = %s AND time = %s", (item['lmtPrice'], item['orderId'], item['time']))

            if 'auxPrice' in item.keys():
                self.cur.execute(f"UPDATE `{self.mysqldb}`.`order` SET aux_price = %s WHERE order_id = %s AND time = %s", (item['auxPrice'], item['orderId'], item['time']))
            self.logger.debug('pending to `order` table')

        if msg_type == 'orderStatus' or msg_type == 'execDetails':
            self.cur.execute(f"""INSERT INTO `{self.mysqldb}`.`order_status` (order_id, perm_id, client_id, filled, last_fill_price, avg_fill_price, remaining, status, time)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                            (item['orderId'], item['permId'], item['clientId'], item['filled'],
                             item['lastFillPrice'], item['avgFillPrice'], item['remaining'], item['status'], item['time']))

            if msg_type == 'execDetails':
                self.cur.execute(f"UPDATE `{self.mysqldb}`.`order_status` SET exec_id = %s, remark = 'execDetails' WHERE order_id = %s AND time = %s", (item['execId'], item['orderId'], item['time']))
            self.logger.debug('pending to `order_status` table')

        if msg_type == 'memory':
            self.cur.execute(f"DELETE FROM `{self.mysqldb}`.`memory`")
            if len(item) > 0:
                for t in list(item.values()):
                    self.cur.execute(f"""INSERT INTO `{self.mysqldb}`.`memory` (order_id, symbol, sec_type, exchange, order_type, action, total_quantity, lmt_price, aux_price, status, time)
                                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                                    (t['orderId'], t['symbol'], t['secType'], t['exchange'], t['orderType'],
                                     t['action'], t['totalQuantity'], t['lmtPrice'], t['auxPrice'], t['status'], t['time']))
                self.logger.debug('pending to `memory` table')

        self.mysql_writer.commit()
        self.logger.debug('written to mysql')

if __name__ == '__main__':
    consumer = OrderConsumer()
    consumer.main()
