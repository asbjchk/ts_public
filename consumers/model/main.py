import os
import json

from common import *

class ModelConsumer(Consumer):
    '''
    Consumer for the model debug data
    '''
    logger = get_logger('model-consumer', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        super().__init__(from_queue=str(os.getenv('FROM_QUEUE', 'model')))
        self.influxdb     = str(os.getenv('INFLUXDB', 'model'))
        self.influxdb_pub = str(os.getenv('INFLUXDB_PUB', 'models')) # for visitor
        self.mysqldb      = str(os.getenv('MYSQLDB', 'position'))
        self.logger.info(f"initialize ModelConsumer: from_queue='{self.from_queue}', influxdb='{self.influxdb}', influxdb_pub='{self.influxdb_pub}', mysqldb='{self.mysqldb}'")

        self.influxdb_writer = self.get_influxdb_writer(database=self.influxdb)
        self.influxdb_pub_writer = self.get_influxdb_writer(database=self.influxdb_pub)
        ## TODO: put the db creation into influxdb init
        self.influxdb_writer.create_database(self.influxdb)
        self.influxdb_writer.create_database(self.influxdb_pub)
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb, username='reader')
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb_pub, username='reader')
        self.influxdb_writer.grant_privilege(privilege='read', database=self.influxdb_pub, username='visitor')
        ##
        self.mysql_writer    = self.get_mysql_writer()
        self.cur = self.mysql_writer.cursor()

    @handle_error(logger_name='model-consumer-error')
    def callback(self, ch, method, properties, body):
        self.logger.debug(f'receive from model queue: {body}')

        msg = json.loads(body)
        msg_type = msg['type']
        item = msg['item']

        if msg_type == 'position':
            data = (item['model'], item['localSymbol'], item['secType'], item['exchange'], item['position'], item['time'], item['position'], item['time'])
            self.logger.debug(f'vector to be inserted into mysql: {data}')
            self.cur.execute(f"""INSERT INTO `{self.mysqldb}`.`model` VALUES (%s, %s, %s, %s, %s, %s)
                                 ON DUPLICATE KEY UPDATE position = %s, time = %s""", data)
            self.mysql_writer.commit()
            self.logger.debug('written to mysql')

        elif msg_type == 'debug':
            info = item['model_info']
            debug = item['debug']
            point = [{'measurement': info['model'],
                      'time': debug.pop('time'),
                      'fields': debug
                    }]
            self.influxdb_writer.write_points(point)
            self.logger.debug(f'written into influxdb: {point}')

            if 'public' in info.keys() and info['public']:
                self.influxdb_pub_writer.write_points(point)
                self.logger.debug(f'written into influxdb (public): {point}')

        else:
            self.logger.warning(f'Message to be handled: {msg}')


if __name__ == '__main__':
    consumer = ModelConsumer()
    consumer.main()
