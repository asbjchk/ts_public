# Trading System developed by Fu-Hong Shiu (John)
Developed in 2018 and continuously maintained and updated. 
This repo is only for demo purposes to illustrate my coding ability. 
I replicated it from my real-trading project (whose GitLab is private) and hid all the sensitive information. 

Below shows the system infrastructure design:
![plot](./system_infra.png)

