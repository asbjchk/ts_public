import logging
import inspect
import pika
import pymysql
import time

from influxdb import InfluxDBClient
from datetime import datetime
from croniter import croniter

def get_logger(name, level='INFO'):
    logger = logging.getLogger(name)
    if not logger.hasHandlers():
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(
            logging.Formatter(
                '[%(asctime)s] [%(name)5s] [%(levelname)8s] [%(filename)10s - %(funcName)10s(): %(lineno)3s] --- %(message)s'
            )
        )
        logger.addHandler(stream_handler)
    logger.setLevel(level)

    return logger

# scheule via cron
def schedule(cron, logger, buffer=10, mute=False):
    """
    A decorator that wraps the passed in function and run
    it according to the cron schedule

    @param cron(str): cron schedule for the function
    @param buffer(float): sleeping buffer time in second
    """
    def decorator(func):
        def scheduler(*args, **kwargs):
            iterator = croniter(cron)
            next_run_time = iterator.get_next(datetime)
            if (next_run_time - datetime.utcnow()).seconds > buffer:
                if not mute:
                    logger.info(f'Next run time: {next_run_time}')
                time.sleep((next_run_time - datetime.utcnow()).seconds - buffer)

            while True:
                if datetime.utcnow() > next_run_time:
                    func(*args, **kwargs)
                    next_run_time = iterator.get_next(datetime)
                    if not mute:
                        logger.info(f'Next run time: {next_run_time}')
                    time.sleep((next_run_time - datetime.utcnow()).seconds - buffer)
        return scheduler
    return decorator

# handle any error raised so that the program continues
def handle_error(logger_name="error-handler"):
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur

    @param logger_name: The name of the logger to be created
    reference: https://www.blog.pythonlibrary.org/2016/06/09/python-how-to-create-an-exception-logging-decorator/
    """
    def decorator(func):
        logger = get_logger(logger_name, level='ERROR')
        logger.handlers[0].setFormatter(
            logging.Formatter(
                '[%(asctime)s] [%(name)5s] [%(levelname)8s] [%(real_filename)10s - %(real_funcName)10s(): %(real_lineno)3s] --- %(message)s'
            )
        )
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                logger.error(e, extra={'real_filename': inspect.trace()[1][1],
                                       'real_funcName': inspect.trace()[1][3],
                                       'real_lineno': inspect.trace()[1][2]})
        return wrapper

    return decorator

class Handler(object):

    def __init__(self):
        pass

    ############################
    ### database connections ###
    ############################
    def get_influxdb_reader(self, database=None):
        return InfluxDBClient('localhost', 8086, username='reader', password='reader', database=database)

    def get_influxdb_writer(self, database=None):
        return InfluxDBClient('localhost', 8086, username='root', password='root', database=database)

    def get_mysql_reader(self):
        return pymysql.connect(host = '127.0.0.1', port = 3306, user = 'reader', passwd = 'reader')

    def get_mysql_writer(self):
        return pymysql.connect(host = '127.0.0.1', port = 3306, user = 'root', passwd = 'root')

    def get_data_schedule(self, date=None):
        if date is None:
            date = datetime.utcnow().strftime('%Y-%m-%d')
        sql_reader = self.get_mysql_writer() # FIXME: should be reader
        cur = sql_reader.cursor()
        cur.execute(f"SELECT * FROM market_data.hsi_schedule WHERE date = '{date}'")
        schedule = cur.fetchall()
        sql_reader.close()

        if len(schedule) > 0:
            schedule = {x[0]: schedule[0][i] for i,x in enumerate(cur.description)}
        else:
            schedule = {}

        return schedule

    #########################
    ### rabbitmq channels ###
    #########################
    def get_rmq_connection(self):
        return pika.BlockingConnection(pika.URLParameters('amqp://root:root@localhost:5672/?heartbeat=0'))

    def get_rmq_consumer_channel(self):
        # NOTE: for safety reason, to test later
        self.consumer_connection = self.get_rmq_connection()
        return self.consumer_connection.channel()

    def get_rmq_publisher_channel(self):
        self.publisher_connection = self.get_rmq_connection()
        return self.publisher_connection.channel()

class Consumer(Handler):

    def __init__(self, from_queue):
        self.from_queue = from_queue
        self.channel = self.get_rmq_consumer_channel()
        self.channel.queue_declare(queue=self.from_queue, durable=True)

    def callback(self, ch, method, properties, body):
        pass

    def main(self):
        self.channel.basic_consume(queue=self.from_queue, on_message_callback=self.callback, auto_ack=True)
        self.channel.start_consuming()

