import os
import json
import time
import pandas as pd

from datetime import timedelta
from threading import Thread

from common import *

class AlertHandler(Handler):
    logger = get_logger('alert-handler', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        self.alert_queue       = str(os.getenv('ALERT_QUEUE', 'test_alert'))
        self.mysql_position_db = str(os.getenv('MYSQL_POSITION_DB', 'real_position'))
        self.check_market_data = str(os.getenv('CHECK_MARKET_DATA')).lower() == 'true'
        self.logger.info(f"initialize Alert Handler: alert_queue='{self.alert_queue}', mysql_position_db='{self.mysql_position_db}', check_market_data={self.check_market_data}")

        # RabbitMQ publisher
        self.channel = self.get_rmq_publisher_channel()
        self.channel.queue_declare(queue=self.alert_queue, durable=True)

        # state
        self.tws_startup_ok = False
        self.market_data_ok = False

    @handle_error(logger_name='alert-handler-error')
    def callback(self, ch, method, properties, body):
        msg = json.loads(body)
#        getattr(self, 'confirm_' + msg['type'])(msg['item'])
        if msg['type'] == 'tws_startup':
            self.tws_startup_ok = True
            self.logger.info(msg['item'])
            self.logger.info('tws startup okay!')

        if msg['type'] == 'market_data':
            self.market_data_ok = True

    def check_position_sync(self):
        SQL_CONNECTION = pymysql.connect(host = '127.0.0.1', port = 3306, user = 'root', passwd = 'root')
        cur = SQL_CONNECTION.cursor()
        cur.execute(f"SELECT * FROM {self.mysql_position_db}.model")
        df_model = pd.DataFrame(cur.fetchall(), columns=[x[0] for x in cur.description])
        cur.execute(f"SELECT * FROM {self.mysql_position_db}.order")
        df_order = pd.DataFrame(cur.fetchall(), columns=[x[0] for x in cur.description])
        SQL_CONNECTION.close()

        df = pd.merge(df_order, df_model, how="outer", on=["model", "exchange", "sec_type", "symbol"], suffixes=['_order', '_model'])
        return all(df.position_order == df.position_model)

    @schedule('* * * * * 0', logger, mute=True) # run every minute
    @handle_error(logger_name='alert-handler-error')
    def reset_states(self):
        """
        reset the following states:
            market_data_ok - every minute
            tws_startup_ok - every 8am HKT
        """
        self.logger.debug('resetting market_data_ok')    
        self.market_data_ok = False
            
        now = datetime.utcnow()
        if now.hour == 0 and now.minute == 0:
            self.logger.debug('resetting tws_startup_ok')
            self.tws_startup_ok = False
            
    
    @schedule('* * * * * 30', logger, mute=True) # run every hh:mm:30
    @handle_error(logger_name='alert-handler-error')
    def check_states_and_trigger_alerts(self):
        """
        check the following states:
            tws_startup_ok = True       , from 08:16:00 HKT to 16:29:59 HKT (for HSI trading)
            market_data_ok = True       , 09:15:00HKT - 11:59:59HKT, 13:00:00HKT - 16:29:59HKT and 17:15:00HKT - 03:00:00HKT (HSI market hour)
            check_position_sync() = True, at any time
        """
        self.logger.debug(f'tws_startup_ok: {self.tws_startup_ok}')
        self.logger.debug(f'market_data_ok: {self.market_data_ok}')
        self.logger.debug(f'check_position_sync(): {self.check_position_sync()}')

        now = datetime.utcnow()
        current_time = str(now.time())
        if current_time > '00:15' and current_time < '08:30':
            self.logger.debug('checking tws_startup_ok')
            if not self.tws_startup_ok:
                self.logger.error("*** TwsApp hasn't successfully connected!")

        if self.check_market_data:
            if (current_time >= '01:15' and current_time < '04:00') or (current_time >= '05:00' and current_time < '08:30') or (current_time >= '09:15' and current_time < '19:00'):
                self.logger.debug('checking market_data_ok')
                if not self.market_data_ok:
                    self.logger.error("*** Market Data disconnected!")

        self.logger.debug('checking check_position_sync()')
        if not self.check_position_sync():
            self.logger.error("*** Position not sync!")
    
    @handle_error(logger_name='alert-handler-error')
    def main(self, date=None):
        if date is None:
            date = datetime.utcnow().strftime('%Y-%m-%d')
        
        self.logger.debug(f'Read data schedule: {date}')
        data_schedule  = self.get_data_schedule(date)

        if len(data_schedule) > 0:
            Thread(target=self.reset_states).start()
            Thread(target=self.check_states_and_trigger_alerts).start()
        
            self.channel.basic_consume(queue=self.alert_queue, on_message_callback=self.callback, auto_ack=True)
            self.channel.start_consuming()

        else:
            self.logger.info('No data schedule for today')

            # sleep until tmr
            now = datetime.utcnow()
            while now.strftime('%Y-%m-%d') == date:
                sleep_time = now.replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1) - now
                sleep_time = sleep_time.seconds
                if sleep_time > 10:
                    self.logger.info(f'Sleep {sleep_time - 10} seconds')
                    time.sleep(sleep_time - 10)
                now = datetime.utcnow()

        self.logger.warning('program exit!')


if __name__ == '__main__':
    handler = AlertHandler()
    handler.main(date=os.getenv('DATE'))
