from base import *

class TOY_HSI(Strategy):
    # NOTE: this is a toy model!
    # class attributes
    info = {'model'    : 'TOY_HSI',
            'symbol'   : 'HSI',
            'secType'  : 'FUT',
            'exchange' : 'HKFE',
            'tif'      : 'DAY',
            'orderType': 'MKT'}

    # instance attributes
    def __init__(self, local_symbol): # e.g. symbol = 'HSIG0'
        # public attributes
        self.info = self.info.copy()
        self.info['localSymbol'] = local_symbol
        self.position = 0
        self.counter = 0
        # protected attributes
        self._entry_bar = 15
        # private attributes
        self.logger.info(f'initialize: {vars(self)}')

    # NOTE: the bar['time'] is the bar start time
    @cache
    @debug
    @handle_error(logger_name='TOY_HSI error')
    def on_bar_update(self, bar):
        self.logger.info(f'bar: {bar}')
        if bar['time'].strftime('%H:%M:%S') == '01:15:00': # TODO: see whether bar-start = 09:14:00 is available
            self.logger.debug('market open')
            self.counter = 0
        if bar['time'].strftime('%H:%M:%S') == '08:00:00':
            self.logger.debug('market close')
            self.exit_long()
            self.exit_short()
            self.counter = 0
        if bar['time'].strftime('%H:%M:%S') > '08:00:00' or bar['time'].strftime('%H:%M:%S') < '01:15:00':
            self.logger.debug('skipped')
            return

        self.counter += 1

        # Entry
        if self.position == 0 and self.counter == self._entry_bar:
            self.enter_long(1)

    def save_debug(self, bar):
        if bar['time'].strftime('%H:%M:%S') >= '01:15:00' and bar['time'].strftime('%H:%M:%S') <= '08:00:00':
            msg = {'model_info': self.info,
                   'debug': {'local_symbol': self.info['localSymbol'],
                             'position'    : int(self.position),
                             'counter'     : int(self.counter),
                             'time'        : datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')}
                  }
            self.logger.debug(f'debug msg: {msg}')
            return msg

    def save_cache(self):
        cache = {'position': self.position,
                 'counter' : self.counter}
        self.logger.debug(f'cache: {cache}')
        json.dump(cache, open(f'__cache__/{self.info["model"]}.json',"w"))
        self.logger.info(f'dumped to __cache__/{self.info["model"]}.json')

    def load_cache(self):
        if os.path.exists(f'__cache__/{self.info["model"]}.json'):
            cache = json.load(open(f'__cache__/{self.info["model"]}.json',"r"))
            for k, v in cache.items():
                vars(self)[k] = v
            self.logger.info(f'loaded cache: {vars(self)}')
        else:
            self.logger.info(f'cache file does not exist, pass')

