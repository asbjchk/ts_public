import os
import json

from importlib import import_module
from datetime import datetime, timedelta

from common import *

class ModelHandler(Handler):
    logger = get_logger('model-handler', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        self.model_queue          = str(os.getenv('MODEL_QUEUE', 'test_model'))
        self.model_to_order_queue = str(os.getenv('MODEL_TO_ORDER_QUEUE', 'test_model_order'))

        self.models = str(os.getenv('MODELS', 'RTO_HSI,PM_HSI'))
        self.models = self.models.split(',') if self.models != '' else []
        self.models_class = self.get_models_class(self.models)

        self.logger.info(f"initialize Model Handler: models={self.models}, model_queue='{self.model_queue}', model_to_order_queue='{self.model_to_order_queue}'")

        # influxdb reader for get_hsi_bar
        self.influxdb_reader = self.get_influxdb_reader(database='market_data')

        # RabbitMQ publisher
        self.channel = self.get_rmq_publisher_channel()
        self.channel.queue_declare(queue=self.model_queue, durable=True)
        self.channel.queue_declare(queue=self.model_to_order_queue, durable=True)

        # cache
        # TODO: save the cache in file and load it on startup
        self.last_bar = {'time': None, 'open': None, 'high': None, 'low': None, 'close': None,
                         'bid_price': None, 'bid_size': None, 'ask_price': None, 'ask_size': None}

    def place_order(self, order: dict):
        """
        place_order implementation for strategies, see model/base.py
        """
        self.channel.basic_publish(body=json.dumps(order), exchange='', routing_key=self.model_to_order_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))

    def publish_to_model_queue(self, msg: dict):
        """
        publish_to_model_queue implementation for strategies, see model/base.py
        """
        self.channel.basic_publish(body=json.dumps(msg), exchange='', routing_key=self.model_queue,
                                   properties=pika.BasicProperties(delivery_mode=2))

    def get_models_class(self, models: list):
        """
        import the <model> class from <model>.py for each <model> in the models list

        @param models(list): models list

        @return classes(list): models' class
        """
        classes = []
        for model in models:
            classes.append(getattr(import_module(model), model))
        return classes

    @handle_error()
    def get_models_instance(self, symbol):
        """
        for each <model> class in the self.models_class,
         - create instance
         - load cache (in ./__cache/)
         - implement place_order
         - implement publish_to_model_queue

        @param symbol(str): contract symbol to be applied by the models

        @ return instances(list): models' instance
        """
        self.logger.debug(f"initializing models' instance on {symbol}...")
        instances = []
        for _class in self.models_class:
            inst = _class(symbol)
            inst.load_cache()
            inst.place_order            = self.place_order
            inst.publish_to_model_queue = self.publish_to_model_queue
            instances.append(inst)
        return instances

    def get_hsi_bar(self, symbol, time=None,
                    last_bar={'time': None, 'open': None, 'high': None, 'low': None, 'close': None,
                              'bid_price': None, 'bid_size': None, 'ask_price': None, 'ask_size': None}):
        '''
        get HSI futures 1-minute bar

        @param symbol(str)          : symbol of the HSI futures contract
        @param time(datetime / str) : time snapshot of the bar, default: None i.e. the current time
        @param last_bar(dict)       : cache bar info for filling missing info

        @return bar(dict): hsi 1-minute bar, containing time, OHLC, last bid/ask price/size
        '''
        if time is None:
            time = 'now()'
        else:
            time = f"'{str(time)}'"

        # load market data
        tmp = self.influxdb_reader.query(f"SELECT first(price) AS open, \
                                             max(price) AS high, \
                                             min(price) AS low, \
                                             last(price) AS close, \
                                             last(bid_price) AS bid_price, \
                                             last(bid_size) AS bid_size, \
                                             last(ask_price) AS ask_price, \
                                             last(ask_size) AS ask_size \
                                      FROM market_data.autogen.\"FUT/HKFE/HSI\" \
                                      WHERE time > {time} - 1m AND time <= {time} AND local_symbol = '{symbol}' \
                                      GROUP BY time(1m)")
        bar = [t for t in tmp['FUT/HKFE/HSI']]

        if len(bar) > 0:
            bar = bar[-1]
            bar['time'] = datetime.strptime(bar['time'], "%Y-%m-%dT%H:%M:%SZ")
        else:
            bar = {'time': None,
                   'open': None, 'high': None, 'low': None, 'close': None,
                   'bid_price': None, 'bid_size': None, 'ask_price': None, 'ask_size': None}
            if time == 'now()':
                bar['time'] = datetime.utcnow()
            else:
                bar['time'] = datetime.strptime(time, "'%Y-%m-%d %H:%M:%S'")
            bar['time'] = bar['time'].replace(microsecond=0, second=0)

        # auto fill bar data missing information
        if bar['open'] is None:
            bar['open']  = last_bar['close']
        if bar['close'] is None:
            bar['close'] = last_bar['close']
        if bar['high'] is None and bar['open'] is not None and bar['close'] is not None:
            bar['high']  = max(bar['open'], bar['close'])
        if bar['low'] is None and bar['open'] is not None and bar['close'] is not None:
            bar['low']   = min(bar['open'], bar['close'])

        if bar['bid_price'] is None:
            bar['bid_price'] = last_bar['bid_price']
        if bar['bid_size'] is None:
            bar['bid_size']  = last_bar['bid_size']
        if bar['ask_price'] is None:
            bar['ask_price'] = last_bar['ask_price']
        if bar['ask_size'] is None:
            bar['ask_size']  = last_bar['ask_size']

        # hotfix: add bar volume
        ## TODO: refactor the code
        try:
            tmp = self.influxdb_reader.query(f"SELECT sum(size) AS volume FROM market_data.autogen.\"FUT/HKFE/HSI\" WHERE time > {time} - 1m AND time <= {time} AND \"tick_by_tick\"='True' AND local_symbol = '{symbol}' GROUP BY time(1m)")
            tmp = [t for t in tmp['FUT/HKFE/HSI']]
            if len(tmp) > 0:
                tmp = tmp[-1]
                if tmp['volume'] is not None:
                    bar['volume'] = tmp['volume']
                else:
                    bar['volume'] = 0
            else:
                bar['volume'] = 0
        except Exception as e:
            self.logger.error(e)

        return bar

#    @schedule('* * * * * 59', logger)
    @schedule('* 0-8 * * 1-5 59', logger)
    @handle_error(logger_name='model-handler-error')
    def run_hsi_models(self, models_instance: list, symbol, time=None):
        """
        run hsi futures models (via the on_bar_update) according to the schedule
        and the hard coded market hour: [08:45 HKT, 12:00 HKT) and [13:00 HKT, 16:30 HKT)

        @param model_instances(list) : model instances to be run
        @param time(datetime / str)  : time snapshot for the model run, default: None i.e. the current time
        """
        bar = self.get_hsi_bar(symbol, last_bar=self.last_bar, time=time)
        self.last_bar = bar

        time = bar['time'].strftime('%H:%M:%S')
        if (time >= '00:45:00' and time < '04:00:00') or (time >= '05:00:00' and time < '08:30:00'):
            for inst in models_instance:
                try:
                    inst.on_bar_update(bar)
                except Exception as e:
                    self.logger.error(e)

    def simulate(self, cron, start_time: datetime, speed=1):
        """
        (for testing purpose) simulate the Model Handler main

        @param start_time(datetime) : start simulated time
        @param speed(float)         : speed of the simulation, e.g. speed=2 means 2x faster
        """
        date = start_time.strftime('%Y-%m-%d')
        self.logger.info(f'Read data schedule: {date}')
        data_schedule  = self.get_data_schedule(date)

        if len(data_schedule) > 0:
            symbol = data_schedule['id_10']
            self.instances = self.get_models_instance(symbol)

            sim_cur_time = start_time
            iterator = croniter(cron, start_time)
            next_run_time = iterator.get_next(datetime)
            if next_run_time > sim_cur_time:
                self.logger.info(f'Next run time: {next_run_time} (simulated current time: {sim_cur_time})')
                time.sleep((next_run_time - sim_cur_time).seconds / speed)
                sim_cur_time = next_run_time

            while True:
                if sim_cur_time >= next_run_time:
                    # run hsi model
                    self.run_hsi_models(models_instance=self.instances, symbol=symbol, time=sim_cur_time)
                    next_run_time = iterator.get_next(datetime)
                    self.logger.info(f'Next run time: {next_run_time} (simulated current time: {sim_cur_time})')
                    time.sleep((next_run_time - sim_cur_time).seconds / speed)
                    sim_cur_time = next_run_time
        else:
            self.logger.info('No data schedule for today')

        self.logger.warning('program exit!')


    @handle_error(logger_name='model-handler-error')
    def main(self):
        """
        main of the whole twsapp
            - Read today data schedule from the mysql
            (if the data schedule is available, i.e. today is a trading day)
                - initialize model instances
                - start running the models
            (else, i.e. today is not a trading day)
                - Sleep the program for whole day
                - Exit the program
        """
        date = datetime.utcnow().strftime('%Y-%m-%d')
        self.logger.info(f'Read data schedule: {date}')
        data_schedule  = self.get_data_schedule(date)

        if len(data_schedule) > 0:
            symbol = data_schedule['id_10']
            self.instances = self.get_models_instance(symbol)

            self.run_hsi_models(models_instance=self.instances, symbol=symbol)

        else:
            self.logger.info('No data schedule for today')

            # sleep until tmr
            now = datetime.utcnow()
            while now.strftime('%Y-%m-%d') == date:
                sleep_time = now.replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1) - now
                sleep_time = sleep_time.seconds
                if sleep_time > 10:
                    self.logger.info(f'Sleep {sleep_time - 10} seconds')
                    time.sleep(sleep_time - 10)
                now = datetime.utcnow()

        self.logger.warning('program exit!')

if __name__ == '__main__':
    handler = ModelHandler()
    handler.main()
