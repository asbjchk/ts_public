import os
import pika
import json
import logging

from datetime import datetime, timedelta
from common import get_logger, handle_error

def debug(func):
    def decorated_func(self, bar):
        func(self, bar)
        debug = self.save_debug(bar)
        if debug is not None:
            self.publish_to_model_queue({'type': 'debug', 'item': debug})

    return decorated_func

def cache(func):
    def decorated_func(self, bar):
        func(self, bar)
        self.save_cache()

    return decorated_func

class Strategy(object):
    info = {'model'    : None,
            'symbol'   : None,
            'secType'  : None,
            'exchange' : None,
            'tif'      : None,
            'orderType': None}

    logger = get_logger('strategy', level=str(os.getenv('LOG_LEVEL', 'INFO')))

    def __init__(self):
        self.position = 0

#     def basic_publish(self, msg, routing_key):
#         '''
#         NOTE: need to implement via the Model Handler
#         e.g. self.channel.basic_publish(body=json.dumps(msg), exchange='', routing_key=routing_key,
#                                         properties=pika.BasicProperties(delivery_mode=2))
#         '''
#         pass

    def place_order(self, order: dict):
        '''
        NOTE: need to implement via the Model Handler

        sample order dict:
            {"model": "StratA",
             "symbol": "HSI",
             "localSymbol": HSIH0",
             "secType": "FUT",
             "exchange": "HKFE",
             "orderType": "MKT",
             "action":"BUY",
             "totalQuantity": 1}
        '''
        pass

    def publish_to_model_queue(self, msg: dict):
        '''
        NOTE: need to implement via the Model Handler

        sample msg dict:
            {"type": "position",
             "msg" : {"model": "StratA",
                      "symbol": "HSI",
                      "localSymbol": HSIH0",
                      "secType": "FUT",
                      "exchange": "HKFE",
                      "orderType": "MKT",
                      "action":"BUY",
                      "totalQuantity": 1,
                      "position": 0,
                      "time": '2020-10-20 03:59:00'}}
        '''
        pass

    def enter_long(self, quantity):
        self.logger.info(f'{self.info["model"]}: enter long, quantity = {quantity} ({self.info["localSymbol"]}) ')
        if quantity > 0:
            msg = self.info.copy()
            msg['action'] = 'BUY'
            msg['totalQuantity'] = quantity
            self.logger.debug(msg)
            self.place_order(order=msg)
            self.position += quantity
            msg['position'] = self.position
            msg['time']     = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
            self.logger.debug(msg)
            #self.basic_publish({'type': 'position', 'item': msg}, 'model')
            self.publish_to_model_queue({'type': 'position', 'item': msg})
        else:
            raise ValueError('The quantity (order size) cannot be zero')


    def enter_short(self, quantity):
        self.logger.info(f'{self.info["model"]}: enter short, quantity = {quantity} ({self.info["localSymbol"]})')
        if quantity > 0:
            msg = self.info.copy()
            msg['action'] = 'SELL'
            msg['totalQuantity'] = quantity
            self.place_order(order=msg)
            self.logger.debug(msg)
            self.position -= quantity
            msg['position'] = self.position
            msg['time']     = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
            #self.basic_publish({'type': 'position', 'item': msg}, 'model')
            self.publish_to_model_queue({'type': 'position', 'item': msg})
        else:
            raise ValueError('The quantity (order size) cannot be zero')

    def exit_long(self):
        self.logger.info(f'{self.info["model"]}: exit long ({self.info["localSymbol"]})')
        if self.position > 0:
            msg = self.info.copy()
            msg['action'] = 'SELL'
            msg['totalQuantity'] = self.position
            self.place_order(order=msg)
            self.logger.debug(msg)
            self.position = 0
            msg['position'] = self.position
            msg['time']     = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
            #self.basic_publish({'type': 'position', 'item': msg}, 'model')
            self.publish_to_model_queue({'type': 'position', 'item': msg})

    def exit_short(self):
        self.logger.info(f'{self.info["model"]}: exit short ({self.info["localSymbol"]}) ')
        if self.position < 0:
            msg = self.info.copy()
            msg['action'] = 'BUY'
            msg['totalQuantity'] = -self.position
            self.place_order(order=msg)
            self.logger.debug(msg)
            self.position = 0
            msg['position'] = self.position
            msg['time']     = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
            #self.basic_publish({'type': 'position', 'item': msg}, 'model')
            self.publish_to_model_queue({'type': 'position', 'item': msg})

    @cache
    @debug
    @handle_error()
    def on_bar_update(self, bar):
        pass

    def save_debug(self):
        pass

    def load_cache(self):
        pass

    def save_cache(self):
        pass

