GRANT SELECT ON `paper_order`.* TO 'reader';
GRANT SELECT ON `paper_position`.* TO 'reader';
GRANT SELECT ON `real_order`.* TO 'reader';
GRANT SELECT ON `real_position`.* TO 'reader';

ALTER USER 'reader'@'%' IDENTIFIED WITH mysql_native_password BY 'reader';
FLUSH PRIVILEGES;
