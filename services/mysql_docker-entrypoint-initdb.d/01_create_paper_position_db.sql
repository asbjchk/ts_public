# Necessary for paper_account-consumer and paper_model-consumer
CREATE DATABASE IF NOT EXISTS `paper_position`;

## Necessary for account-consumer
CREATE TABLE IF NOT EXISTS `paper_position`.`account_total`
     (time timestamp(6),
      symbol varchar(255),
      sec_type varchar(255),
      exchange varchar(255),
      position float
);

## Necessary for model-consumer
CREATE TABLE IF NOT EXISTS `paper_position`.`model` \
    (model varchar(100), \
     symbol varchar(100), \
     sec_type varchar(100), \
     exchange varchar(100), \
     position float, \
     time timestamp(6), \
     PRIMARY KEY (model, exchange, sec_type, symbol) \
);

DELIMITER //
CREATE PROCEDURE `paper_position`.`calc_position_from_order_db` () 
BEGIN 
    DROP TEMPORARY TABLE IF EXISTS `paper_position`.`position_from_order_db`; 
    CREATE TEMPORARY TABLE `paper_position`.`position_from_order_db` 
        SELECT `tmp`.`model`, `tmp`.`symbol`, `tmp`.`sec_type`, `tmp`.`exchange`, SUM(`tmp`.`position`) AS `position`, MAX(`tmp`.`time`) AS `time` 
        FROM (SELECT `model`, `action`, `symbol`, `sec_type`, `exchange`, IF(`action` = 'BUY', SUM(`total_quantity`), -SUM(`total_quantity`)) AS `position`, MAX(`time`) AS `time` 
              FROM `paper_order`.`filled_orders_by_model` 
              GROUP BY `model`, `action`, `symbol`, `sec_type`, `exchange` 
             ) AS `tmp` 
        GROUP BY `tmp`.`model`, `tmp`.`symbol`, `tmp`.`sec_type`, `tmp`.`exchange`; 
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE `paper_position`.`update_position_order_table` ()
BEGIN
    CALL `paper_order`.`calc_filled_orders_by_model`();
    CALL `paper_position`.`calc_position_from_order_db`();
    CREATE TABLE IF NOT EXISTS `paper_position`.`order` 
        (
            model varchar(100),
            symbol varchar(100),
            sec_type varchar(100),
            exchange varchar(100),
            position float,
            time timestamp(6),
            PRIMARY KEY (model, exchange, sec_type, symbol)
        );
    INSERT INTO `paper_position`.`order`
        SELECT `tmp`.* FROM `paper_position`.`position_from_order_db` AS `tmp`
    ON DUPLICATE KEY UPDATE `position` = `tmp`.`position`, `time` = `tmp`.`time`;
END//
DELIMITER ;

DELIMITER //
CREATE EVENT `paper_position`.`update_position_order_table` ON SCHEDULE EVERY 10 SECOND DO
BEGIN
    CALL `paper_position`.`update_position_order_table`();
END//
DELIMITER ;
