# Necessary for real_order-consumer
CREATE DATABASE IF NOT EXISTS `real_order`;

CREATE TABLE IF NOT EXISTS `real_order`.`model` \
( \
    order_id int, \
    model varchar(255), \
    time timestamp(6), \
    PRIMARY KEY (time, order_id) \
);

CREATE TABLE IF NOT EXISTS `real_order`.`order` \
( \
    order_id int, \
    symbol varchar(255), \
    sec_type varchar(255), \
    exchange varchar(255), \
    tif varchar(255), \
    order_type varchar(255), \
    action varchar(255), \
    total_quantity int, \
    lmt_price float, \
    aux_price float, \
    status varchar(255), \
    time timestamp(6), \
    PRIMARY KEY (time, order_id) \
);

CREATE TABLE IF NOT EXISTS `real_order`.`order_status` \
( \
    order_id int, \
    exec_id text, \
    perm_id int, \
    client_id int, \
    filled int, \
    last_fill_price float, \
    avg_fill_price float, \
    remaining int, \
    status varchar(255), \
    time timestamp(6), \
    remark varchar(255), \
    PRIMARY KEY (time, order_id) \
);

CREATE TABLE IF NOT EXISTS `real_order`.`memory` \
( \
    order_id int, \
    symbol varchar(255), \
    sec_type varchar(255), \
    exchange varchar(255), \
    order_type varchar(255), \
    action varchar(255), \
    total_quantity int, \
    lmt_price float, \
    aux_price float, \
    status varchar(255), \
    time timestamp(6), \
    PRIMARY KEY (order_id) \
);

DELIMITER //
CREATE PROCEDURE `real_order`.`calc_filled_orders_by_model`()
BEGIN
    DROP TEMPORARY TABLE IF EXISTS `real_order`.`filled_orders_by_model`;
    CREATE TEMPORARY TABLE `real_order`.`filled_orders_by_model`
    SELECT `x`.`date`,`x`.`model`, `x`.`order_id`, `y`.`symbol`, `y`.`sec_type`, `y`.`exchange`, `y`.`action`, `y`.`total_quantity`, `y`.`time`
    FROM (SELECT `a`.`order_id`, `a`.`model`, date(`a`.`time`) AS `date`
          FROM `real_order`.`model` AS `a`
          WHERE `a`.`time` = (SELECT  MAX(`time`) AS `time` FROM `real_order`.`model` AS `b` WHERE `b`.`order_id` = `a`.`order_id` AND date(`b`.`time`) = date(`a`.`time`))
         ) AS `x`
         INNER JOIN
         (SELECT `a`.`order_id`, `a`.`symbol`, `a`.`sec_type`, `a`.`exchange`, `a`.`total_quantity`, `a`.`action`, `a`.`time`, date(`a`.`time`) AS `date`
          FROM `real_order`.`order` AS `a`
          WHERE `a`.`time` = (SELECT  MAX(`time`) AS `time` FROM `real_order`.`order` AS `b` WHERE `b`.`order_id` = `a`.`order_id` AND date(`b`.`time`) = date(`a`.`time`) AND (`b`.`status` = 'Filled' OR `b`.`status` = 'Filled (internal)'))
         ) AS `y`
         ON `x`.`order_id` = `y`.`order_id` AND `x`.`date` = `y`.`date`;
END//
DELIMITER ;
